<?php

namespace App\Http\Controllers;

use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Models\User;
use Carbon\Carbon;
use F9Web\ApiResponseHelpers;
use Illuminate\Http\JsonResponse;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

/**
 * Password management
 *
 * To Help user to reset their password in case of needed
 *
 */
class ForgotPasswordController extends Controller
{
    use ApiResponseHelpers;

    /**
     * Forgot password request
     *
     * @param ForgotPasswordRequest $request
     * @return JsonResponse
     */
    public function forgot(ForgotPasswordRequest $request): JsonResponse
    {
        $email = $request->input('email');

        if (User::where('email', $email)->doesntExist()){
            return $this->respondNotFound('Email address not valid.');
        }

        $token = Str::random(25);

        DB::table('password_resets')->insert([
            'email' => $email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        Mail::send('Mails.forgot', ['token' => $token], function (Message $message) use ($email){
            $message->to($email);
            $message->subject('Reset your password');
        });

        return $this->respondOk('Check your mailbox for the next step.');
    }

    /**
     * Reset password from mailbox
     *
     * @bodyparam String $token
     * @param ResetPasswordRequest $request
     * @return JsonResponse
     */
    public function reset(ResetPasswordRequest $request): JsonResponse
    {
        $inputs = $request->all();

        // Check if that provide token is valid in database
        if (!$passwordResets = DB::table('password_resets')->where('token', $inputs['token'])->first()){
            return $this->respondNotFound('No password request founded.');
        }

        // Check if that email address is exist
        /** @var User $user */
        if (!$user = User::where('email', $passwordResets->email)->first()){
            return $this->respondNotFound('User not found.');
        }

        $user->update([
            'password'=> Hash::make($inputs['password'])
        ]);

        DB::table('password_resets')
            ->where(['email' => $passwordResets->email])
            ->delete();

        return $this->respondOk('Password reset successfully.');
    }
}
