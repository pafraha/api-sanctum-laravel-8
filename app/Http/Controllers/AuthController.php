<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Exception;
use F9Web\ApiResponseHelpers;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

/**
 * @group User management
 *
 * APIs Laravel Sanctum
 */
class AuthController extends Controller
{

    // Testing new package from this trait file
    use ApiResponseHelpers;

    /**
     * Create a new user
     *
     * This endpoint lets you create a user
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        $input = $request->all();

        try {
            /** @var User $user */
            $user = User::create([
                'name' => $input['name'],
                'email' => $input['email'],
                'password' => Hash::make($input['password'])
            ]);

            $token = $user->createToken('apps.dev')->plainTextToken;

            return $this->respondWithSuccess([
                'token' => $token,
                'user' => $user
            ]);
        } catch (ValidationException $exception){
            return $this->respondError($exception->getMessage());
        }
    }

    /**
     * To login a user with email and password
     *
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $inputs = $request->all();

        try {
            // Check email
            if (!$user = User::where('email', $inputs['email'])->first()) {
                return $this->respondNotFound('Email address not found.');
            }

            // Check password
            if(!$user || !Hash::check($inputs['password'], $user->password)) {
                return $this->respondUnAuthenticated('Invalid credentials.');
            }

            $token = $user->createToken('apps.dev')->plainTextToken;

            return $this->respondWithSuccess([
                'user' => $user,
                'token' => $token
            ]);
        } catch (Exception $exception){
            return $this->respondError($exception->getMessage());
        }
    }

    /**
     * To logout a user
     *
     */
    public function logout(): JsonResponse
    {
        Auth::user()->tokens()->delete();

        return $this->respondOk('You\re now sign out.');
    }

    /**
     * Change password for a user
     *
     * @param ChangePasswordRequest $request
     * @return JsonResponse
     */
    public function change(ChangePasswordRequest $request): JsonResponse
    {
        $inputs = $request->all();

        // Check if the password provided is matching with the current password
        if (!Hash::check($inputs['old_password'], Auth::user()->getAuthPassword())){
            return $this->respondFailedValidation('Your current password does not match with the given password.');
        }

        // Update new password to database
        User::find(Auth::id())->update([
                'password' => Hash::make($inputs['password'])
            ]);

        return $this->respondOk('Password changed.');
    }
}
