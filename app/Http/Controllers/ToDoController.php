<?php

namespace App\Http\Controllers;

use App\Models\ToDo;
use F9Web\ApiResponseHelpers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ToDoController extends Controller
{

    use ApiResponseHelpers;

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->respondWithSuccess([
            'tasks' => ToDo::all()
        ]);
    }

    public function taskByUser($user_id)
    {
        return $this->respondWithSuccess([
            'tasks' => ToDo::where('user_id', $user_id)->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'task' => 'required',
                'user_id' => 'required'
            ]);

            ToDo::create([
                'task' => $request->input('task'),
                'status' => 'To-Do',
                'user_id' => $request->input('user_id')
            ]);

            return $this->respondOk('New task added successfully.');
        } catch (\Exception $exception){
            return $this->respondUnAuthenticated($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return ToDo::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $task = ToDo::find($id);

        return $task->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return int
     */
    public function destroy($id)
    {
        return ToDo::destroy($id);
    }

    /**
     * Search by username
     *
     * @param integer $user_id
     * @return Response
     */
    public function search(int $user_id)
    {
        return ToDo::where('user_id', $user_id)->get();
    }
}
