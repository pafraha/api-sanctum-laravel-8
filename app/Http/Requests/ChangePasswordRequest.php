<?php

namespace App\Http\Requests;

use App\Rules\allowedSymbol;
use App\Rules\withOutSpace;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

/**
 * @property mixed $old_password
 */
class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => ['required', Password::default()->symbols()->numbers()->mixedCase()->letters()->uncompromised(),
                                                        new withOutSpace(), new allowedSymbol()],
            'password_confirm' => 'required|same:password'
        ];
    }
}
