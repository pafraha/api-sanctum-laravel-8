# Laravel 8 - REST API with Sanctum

This is a REST API using auth tokens with Laravel Sanctum for backend use.

## Usage

Change the *.env.example* to *.env* and add your database info

```
# Run the webserver on port 8000
php artisan serve
```

## Routes

```
# Public

POST   /api/login
@body: email, password

POST   /api/reset/{token}
@body: email, password, password_confirm, token

POST   /api/forgot
@body: email

POST   /api/register
@body: name, email, password, password_confirm

GET   /api/products
GET   /api/products/:id

# Protected

POST   /api/products
@body: name, slug, description, price

PUT   /api/products/:id
@body: ?name, ?slug, ?description, ?price

DELETE  /api/products/:id

POST    /api/logout
```
