<?php

use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ToDoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use F9Web\ApiResponseHelpers;

// Public routes
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::post('/reset/{token}', [ForgotPasswordController::class, 'reset']);
Route::post('/forgot', [ForgotPasswordController::class, 'forgot']);

Route::resource('tasks', ToDoController::class);
Route::get('/task/{user_id}', [ToDoController::class, 'taskByUser']);

Route::get('/login', function (){
    return json_encode([
        'error' => 'You\'re not connected.'
    ], JSON_THROW_ON_ERROR);
});

Route::get('/products', [ProductController::class, 'index']);
Route::get('/products/{id}', [ProductController::class, 'show']);
Route::get('/products/search/{name}', [ProductController::class, 'search']);

// Protected routes
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/change', [AuthController::class, 'change']);
    Route::get('/user', function (Request $request){
        return $request->user();
    });

    Route::post('/products', [ProductController::class, 'store']);
    Route::put('/products/{id}', [ProductController::class, 'update']);
    Route::delete('/products/{id}', [ProductController::class, 'destroy']);
});
